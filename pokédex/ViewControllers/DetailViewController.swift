//
//  DetailViewController.swift
//  pokédex
//
//  Created by David Polania on 10/14/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import UIKit
import RxSwift
import AlamofireImage

class DetailViewController: UIViewController {
    
    @IBOutlet weak var imagePokemon: UIImageView!
    @IBOutlet weak var namePokemon: detailLabel!
    @IBOutlet weak var textDetail: UITextView!
    @IBOutlet weak var labelTitleExperience: detailLabel!
    @IBOutlet weak var labelDescriptionExperience: detailLabel!
    var pokemon : pokemonInfoModel?
    let pokemonviewmodel = pokemonViewModel()
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareCallbacks()
        pokemonviewmodel.getPokemontSpecies(url: (pokemon?.species?.url)!)
        // Do any additional setup after loading the view.
    }
    
    
    private func prepareCallbacks() {
        pokemonviewmodel.onShowError.subscribe { (error) in
            DismissPopUp(viewcont: self)
            print(error.element?.status_message! ?? "")
        }.disposed(by: disposeBag)
        
        pokemonviewmodel.onNavigate.subscribe(onNext: { (result) in
            self.textDetail.text = result.flavor?.filter{$0.language?.name == "es"}.first?.text
            self.namePokemon.text = self.pokemon?.name
            self.labelDescriptionExperience.text = String(self.pokemon?.base_experience ?? 0)
            let url = URL(string: "\(String((self.pokemon?.sprites?.front_default)!))") ?? URL(string: "")!
            let filter = AspectScaledToFillSizeFilter(
                size:self.imagePokemon.frame.size
            )
            (self.imagePokemon).af_setImage(withURL: url, placeholderImage: nil,filter:filter)
            DismissPopUp(viewcont: self)
        }, onError: { (Error) in
            DismissPopUp(viewcont: self)
            print(Error)
        }, onCompleted: {
             DismissPopUp(viewcont: self)
            print("complete")
        }).disposed(by: self.disposeBag)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
