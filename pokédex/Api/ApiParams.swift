//
//  ApiParams.swift
//  pokédex
//
//  Created by David Polania on 10/14/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import Foundation

struct ApiParams {

    static let baseURL = "https://pokeapi.co/api/v2/"
}

struct ApiServices {
    static let pokemon = "\(ApiParams.baseURL)pokemon/"
}

//Header fields
enum HttpHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
}

//Content Type (JSON)
enum ContentType: String {
    case json = "application/json"
}

enum AFAApiError2: Error {
    case forbidden(Int,String)   //Status code 403
    case notFound               //Status code 404
    case conflict               //Status code 409
    case internalServerError    //Status code 500
}
