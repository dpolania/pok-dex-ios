//
//  DetailLabel.swift
//  pokédex
//
//  Created by David Polania on 10/14/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import Foundation
import UIKit

class detailLabel: UILabel {
    required init?(coder aDecoder: NSCoder) {
           super.init(coder: aDecoder)
        self.font = UIFont(name:FontUtil.FontDetail, size: CGFloat(FontSizeUtil.Detail))
           self.textColor = AppColorUtil.TextColor
       }

}
