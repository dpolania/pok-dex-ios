//
//  CurveView.swift
//  pokédex
//
//  Created by David Polania on 10/14/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import UIKit

class CurveView: UIView {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.cornerRadius = 15.0
        self.layer.borderWidth = 1
        self.layer.masksToBounds = true
    }
}
