//
//  spritesModel.swift
//  pokédex
//
//  Created by David Polania on 10/14/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import Foundation

struct spritesModel : Codable {
    var front_default : String?
    /*var front_shiny : String?
    var front_female : String?
    var front_shiny_female : String?
    var back_default : String?
    var back_shiny : String?
    var back_female : String?
    var back_shiny_female : String?*/
    

    //Adding custom keys
    private enum CodingKeys: String, CodingKey {
        case front_default = "front_default"
        /*case front_shiny = "front_shiny"
        case front_female = "front_female"
        case front_shiny_female = "front_shiny_female"
        case back_default = "back_default"
        case back_shiny = "back_shiny"
        case back_female = "back_female"
        case back_shiny_female = "back_shiny_female"*/
    }
}
