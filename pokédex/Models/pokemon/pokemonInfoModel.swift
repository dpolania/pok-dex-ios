//
//  pokemonInfoModel.swift
//  pokédex
//
//  Created by David Polania on 10/14/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import Foundation
struct pokemonInfoModel : Codable{
    
    let id : Int?
    let name : String?
    let base_experience : Int?
    let height : Int?
    let sprites : spritesModel?
    let species : pokemonResultModel?
    
    //Adding custom keys
    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case base_experience = "base_experience"
        case height = "height"
        case sprites = "sprites"
        case species = "species"
    }
}
