//
//  FontUtil.swift
//  pokédex
//
//  Created by David Polania on 10/14/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import Foundation
struct FontUtil {
    static let FontPrimary = "Futura-Bold"
    static let FontDetail = "Futura-MediumItalic"
}
