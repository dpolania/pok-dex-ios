//
//  PresentPopUp.swift
//  pokédex
//
//  Created by David Polania on 10/14/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import Foundation
import UIKit

var popupLoginer : UIViewController = UIViewController()
func PresentPopUp(viewcont : UIViewController){
    let popupLoginer = PopuLoginerViewController(nibName: "PopuLoginerViewController", bundle: nil)
    popupLoginer.view.tag = 999
    viewcont.view.addSubview(popupLoginer.view)
    
    //popupLoginer.modalPresentationStyle = .overCurrentContext
    //viewcont.present(popupLoginer, animated: true, completion: nil)
}

func DismissPopUp(viewcont : UIViewController){
    let view = viewcont.view.viewWithTag(999)
    view?.removeFromSuperview()
    //PopuLoginerViewController.onDissmis!("ok")
}

