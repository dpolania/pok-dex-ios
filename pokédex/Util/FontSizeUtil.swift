//
//  FontSizeUtil.swift
//  pokédex
//
//  Created by David Polania on 10/14/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import Foundation
struct FontSizeUtil {
    static let Title = 23.0
    static let Description = 15.0
    static let Paragraph = 16.0
    static let Detail = 8.0
}
