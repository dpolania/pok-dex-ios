//
//  ViewController.swift
//  pokédex
//
//  Created by David Polania on 10/14/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import UIKit
import RxSwift
import AlamofireImage

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var pokemonsTable: UITableView!
    
    var pokemons = [pokemonInfoModel]()
    let pokemonviewmodel = pokemonViewModel()
    let textCellIdentifier = "pokemonsTableViewCell"
    private let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        pokemonviewmodel.getPokemonts()
        prepareCallbacks()
        registerTableViewCell()
        pokemonsTable.dataSource = self
        pokemonsTable.delegate = self
        // Do any additional setup after loading the view.
    }
    private func prepareCallbacks() {
        pokemonviewmodel.onShowError.subscribe { (error) in
            DismissPopUp(viewcont: self)
            print(error.element?.status_message! ?? "")
        }.disposed(by: disposeBag)
        
        pokemonviewmodel.onNavigateBack.subscribe(onNext: { (result) in
            self.pokemons = result
            self.pokemonsTable.reloadData()
            DismissPopUp(viewcont: self)
        }, onError: { (Error) in
            DismissPopUp(viewcont: self)
            print(Error)
        }, onCompleted: {
             DismissPopUp(viewcont: self)
            print("complete")
        }).disposed(by: self.disposeBag)
    }
    private func registerTableViewCell() {
        let cell = UINib(nibName: "pokemonsTableViewCell", bundle: nil)
        self.pokemonsTable.register(cell, forCellReuseIdentifier: "pokemonsTableViewCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: textCellIdentifier, for: indexPath) as! pokemonsTableViewCell
        cell.titleImage.text = pokemons[indexPath.row].name
        let url = URL(string: "\(String((pokemons[indexPath.row].sprites?.front_default)!))") ?? URL(string: "")!
        let filter = AspectScaledToFillSizeFilter(
            size:cell.identifyImage.frame.size
        )
        (cell.identifyImage).af_setImage(withURL: url, placeholderImage: nil,filter:filter)
        cell.pageLabel.text = String(pokemons[indexPath.row].id ?? 0)
        cell.pokemon = pokemons[indexPath.row]
        cell.onSelect = { Result in
            let controller = self.storyboard?.instantiateViewController(identifier: "DetailViewController") as! DetailViewController
            controller.pokemon = Result
            self.navigationController?.pushViewController(controller, animated: true)
        }
        return cell
    }

}

