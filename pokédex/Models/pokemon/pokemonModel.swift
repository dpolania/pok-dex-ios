//
//  pokemonModel.swift
//  pokédex
//
//  Created by David Polania on 10/14/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import Foundation

struct pokemonModel : Codable {
    let count: Int?
    let next: String?
    let previous: String?
    let results: [pokemonResultModel]?
   
    //Adding custom keys
    private enum CodingKeys: String, CodingKey {
        case count = "count"
        case next = "next"
        case previous = "previous"
        case results = "results"
    }
}
