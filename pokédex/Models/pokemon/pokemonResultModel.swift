//
//  pokemonResultModel.swift
//  pokédex
//
//  Created by David Polania on 10/14/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import Foundation

struct pokemonResultModel: Codable  {
    let name: String?
    let url: String?
   
    //Adding custom keys
    private enum CodingKeys: String, CodingKey {
        case name = "name"
        case url = "url"
    }
}
