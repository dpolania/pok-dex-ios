//
//  SpeciesModel.swift
//  pokédex
//
//  Created by David Polania on 10/14/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import Foundation

struct SpeciesModel : Codable{
    let flavor : [flavorEntriesModel]?
    
    private enum CodingKeys: String, CodingKey {
        case flavor = "flavor_text_entries"
    }
}
