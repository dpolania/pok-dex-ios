//
//  flavorEntriesModel.swift
//  pokédex
//
//  Created by David Polania on 10/14/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import Foundation
struct flavorEntriesModel : Codable {
    let text : String?
    let language : pokemonResultModel?
    
    private enum CodingKeys: String, CodingKey {
        case text = "flavor_text"
        case  language = "language"
    }
}
