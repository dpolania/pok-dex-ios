//
//  pokemonViewModel.swift
//  pokédex
//
//  Created by David Polania on 10/14/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import Foundation
import RxSwift


class pokemonViewModel{
    //MARK: - Properties
    private let apiManager: ApiManager
    let disposeBag = DisposeBag()
    
    let onNavigateBack = PublishSubject<[pokemonInfoModel]>()
    let onShowError = PublishSubject<ErrorModel>()
    let onShowSuccess = PublishSubject<pokemonModel>()
    let onNavigate = PublishSubject<SpeciesModel>()
    
    var countConsume: Int = 0
    var pokemons = [pokemonInfoModel]()
    var statusData = [[String:String]]()
    
    //MARK: - Injection ApiManager
    init(apiManager: ApiManager = ApiManager()) {
        self.apiManager = apiManager
    }

    
    func getPokemonts(){
        self.apiManager.Get(url:ApiServices.pokemon).subscribe(onNext: { response in
            print(response)
            do { //Converting data to Object
                let responseDecode = try JSONDecoder().decode(pokemonModel.self, from: response)
                for item in responseDecode.results ?? [pokemonResultModel](){
                    self.statusData.append([String(item.name ?? "") :""])
                    self.apiManager.Get(url: "\(String(describing: item.url ?? ""))").subscribe(onNext: { response in
                        do{
                            let data = try JSONDecoder().decode(pokemonInfoModel.self, from: response)
                            self.pokemons.append(data)
                            if self.pokemons.count == self.statusData.count{
                                self.onNavigateBack.onNext(self.pokemons)
                                print("ok pokemons")
                            }
                        }
                        catch let error {
                            print(error)
                            let resultError = ReceiverStatus(response: error)
                            self.onShowError.onNext(resultError)
                        }
                    }, onError: { (error) in
                        let resultError = ReceiverStatus(response: error)
                        self.onShowError.onNext(resultError)
                    }, onCompleted: {
                        print("onCompleted")
                    }).disposed(by: self.disposeBag)
                }
            } catch let error {
                print(error)
                let resultError = ReceiverStatus(response: error)
                self.onShowError.onNext(resultError)
            }
        }, onError: { (error) in
            let resultError = ReceiverStatus(response: error)
            self.onShowError.onNext(resultError)
        }, onCompleted: {
            print("onCompleted")
        }).disposed(by: disposeBag)
    }
    func getPokemontSpecies(url:String){
        self.apiManager.Get(url:url).subscribe(onNext: { response in
            print(response)
            do{
                let data = try JSONDecoder().decode(SpeciesModel.self, from: response)
                if self.pokemons.count == self.statusData.count{
                    self.onNavigate.onNext(data)
                }
            }
            catch let error {
                print(error)
                let resultError = ReceiverStatus(response: error)
                self.onShowError.onNext(resultError)
            }
        }, onError: { (error) in
            let resultError = ReceiverStatus(response: error)
            self.onShowError.onNext(resultError)
        }, onCompleted: {
            print("onCompleted")
        }).disposed(by: disposeBag)
    }
}
