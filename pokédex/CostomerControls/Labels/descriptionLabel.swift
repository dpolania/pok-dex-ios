//
//  descriptionLabel.swift
//  pokédex
//
//  Created by David Polania on 10/14/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import UIKit

class descriptionLabel: UILabel {
    required init?(coder aDecoder: NSCoder) {
           super.init(coder: aDecoder)
        self.font = UIFont(name:FontUtil.FontPrimary, size: CGFloat(FontSizeUtil.Title))
           self.textColor = AppColorUtil.TextColor
       }

}
