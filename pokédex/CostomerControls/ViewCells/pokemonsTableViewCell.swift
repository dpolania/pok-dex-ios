//
//  pokemonsTableViewCell.swift
//  pokédex
//
//  Created by David Polania on 10/14/19.
//  Copyright © 2019 David Polania. All rights reserved.
//

import UIKit

class pokemonsTableViewCell: UITableViewCell {

    @IBOutlet weak var identifyImage: UIImageView!
    @IBOutlet weak var titleImage: descriptionLabel!
    @IBOutlet weak var pageLabel: detailLabel!
    var onSelect:((_ Result:pokemonInfoModel)->())?
    var pokemon : pokemonInfoModel?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func eventSelected(_ sender: Any) {
        onSelect!(pokemon!)
    }
}
